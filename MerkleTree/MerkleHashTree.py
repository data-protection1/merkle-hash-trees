import hashlib

class MerkleHashTree(object):
    """docstring for MerkleHastTree."""
    def __init__(self, documents):
        self.documents = self._Documents(documents)
        self.root = self.Node('root')
        self.heigth = self._getHeigth()
        print(self.heigth)
        # the recursive call to build the tree
        self.root.buildTree(self.documents,self.heigth)

    def _getHeigth(self):
        size = self.documents.size
        if(size % 2 == 0):
            return len("{0:b}".format(size))
        else:
            return  len("{0:b}".format(size + 1))

    def printTree(self):
        self.root.printTree()


    def checkIfExists(self,document,index):
        #transform the index to binari to able find the leaf. we will be swifting numbers until just remind one
        index = format(index, '0'+str(self.heigth)+'b')
        inputHash = self.documents.hashDocument(document)
        return self.root.checkDocument(inputHash,index)

    def insertData(self,document):
        self.documents.insertDocument(document)
        self.root = self.Node('root')
        self.heigth = self._getHeigth()
        self.root.buildTree(self.documents,self.heigth)

    class Node(object):
        def __init__(self,  name, data = None, rigth = None, left = None, parent = None):
            self.name   = name
            self.parent = parent
            self.rigth  = rigth
            self.left   = left
            self.data   = data

        def buildTree(self,documents,heigth):
            #if the current node return the hash recursively
            if (len(self.name) == heigth -1):
                self._computeHash(documents)
            else:
                #get the name of the son's, 0 at the start indicates left 1 rigth
                self.left = MerkleHashTree.Node(self._getName(0))
                self.left.buildTree(documents,heigth)
                self.rigth = MerkleHashTree.Node(self._getName(1))
                self.rigth.buildTree(documents,heigth)
                self._computeHash(documents)

        def checkDocument(self,candidateHash,index):
            if(self.isLeaf()):
                #check if the leaf have the same hash
                checkLeaf = (candidateHash == self.data)
                return checkLeaf, self.data
            else:
                if(index[0] == '1'):
                    sonValue, rightHash = self.getRight().checkDocument(candidateHash, index[1:])
                    leftHash = self.getLeft().data
                else:
                    sonValue, leftHash = self.getLeft().checkDocument(candidateHash, index[1:])
                    rightHash = self.getLeft().data
                if sonValue:
                    return self.checkCurrentNode(leftHash, rightHash)
                else:
                    return False, None

        def checkCurrentNode(self,rightHash, leftHash):
            #check if the sons hash using the candidate match's with the original one
            return (self.data == self._hash(rightHash + leftHash), self.data)


        def _getName(self,side):
            if(self.name == 'root'):
                if(side == 0):
                    return self._nameLeft('')
                else:
                    return self._nameRight('')
            else:
                if(side == 0):
                    return self._nameLeft(self.name)
                else:
                    return self._nameRight(self.name)


        def _nameLeft(self,name):
            return name + '0'

        def _nameRight(self,name):
            return name + '1'

        def _hash(self,data):
            sha256 = hashlib.sha256()
            sha256.update(str(data).encode('utf-8'))
            return sha256.hexdigest()

        def _computeHash(self,documents):
            if self.isLeaf():
                try:
                    self.data = documents.getHashDocument(int(self.name,2))
                except IndexError:
                    self.data = documents.getEmptyhash()
            else:
                print(self)
                self.data = self._hash(self.getRight().data + self.getLeft().data)

        def isLeaf(self):
            return  self.rigth == None and self.left == None

        def getRight(self):
            return self.rigth

        def getLeft(self):
            return self.left

        def getHash(self):
            return self.data

        def printTree(self):
            if(self.isLeaf()):
                print(self.name, self.data)
            else:
                self.left.printTree()
                self.rigth.printTree()
                print(self.name, self.data)

    class _Documents():
        def __init__(self, documents):
            self.data = []
            self.size = 0
            self._storeDocuments(documents)

        def _storeDocuments(self, documents):
            for x in range(0,len(documents)):
                self.size += 1
                self.data.append(documents[x])

        def _storeDocument(self,document):
            self.data.append(document)

        def insertDocument(self,document):
            self.size += 1
            self._storeDocument(document)

        def getHashDocument(self, index):
            sha256 = hashlib.sha256()
            with open(self.data[index], 'rb') as f:
                while True:
                    data = f.read(4096)
                    if not data:
                        break
                    sha256.update(data)
            return sha256.hexdigest()

        def hashDocument(self, document):
            sha256 = hashlib.sha256()
            with open(document, 'rb') as f:
                while True:
                    data = f.read(4096)
                    if not data:
                        break
                    sha256.update(data)
            return sha256.hexdigest()


        def getEmptyhash(self):
            sha256 = hashlib.sha256()
            return sha256.hexdigest()

        def __getitem__(self,n):
            return list(self.data.keys())[n]

__version__ = "0.2.0"
__author__ = "Eric Monné Mesalles"
__license__ = "GNU GPLv3"

from .MerkleHashTree import MerkleHashTree
